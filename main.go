/*
Copyright 2017 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.oldworldcomputing.com/singularity/server/k8s-event-exporter/sinks/elasticsearch"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"github.com/peterbourgon/ff/v3"
)

func newSystemStopChannel() chan struct{} {
	ch := make(chan struct{})
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		sig := <-c
		glog.Infof("Received signal %s, terminating", sig.String())

		// Close stop channel to make sure every goroutine will receive stop signal.
		close(ch)
	}()

	return ch
}

func newKubernetesClient() (kubernetes.Interface, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("failed to create in-cluster config: %v", err)
	}
	// Use protobufs for communication with apiserver.
	config.ContentType = "application/vnd.kubernetes.protobuf"

	return kubernetes.NewForConfig(config)
}

func main() {
	fs := flag.NewFlagSet("event-exporter", flag.ContinueOnError)
	var (
		namespace             = fs.String("namespace", "default", "Namespace to watch for jobs")
		resyncPeriod          = fs.Duration("resync-period", 15*time.Second, "Reflector resync period")
		prometheusEndpoint    = fs.String("prometheus-endpoint", ":80", "Endpoint on which to expose Prometheus http handler")
		elasticsearchEndpoint = fs.String("elasticsearch-server", "http://elasticsearch:9200/", "Elasticsearch endpoint")
	)

	flag.Set("logtostderr", "true")
	defer glog.Flush()

	if err := ff.Parse(fs, os.Args[1:], ff.WithEnvVars()); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	config := elasticsearch.DefaultElasticSearchConf()
	config.Endpoint = *elasticsearchEndpoint
	sink, err := elasticsearch.NewElasticSearchSink(config)
	if err != nil {
		glog.Fatalf("Failed to initialize elasticsearch output: %v", err)
	}

	client, err := newKubernetesClient()
	if err != nil {
		glog.Fatalf("Failed to initialize kubernetes client: %v", err)
	}

	eventExporter := newEventExporter(client, sink, *resyncPeriod, *namespace)

	// Expose the Prometheus http endpoint
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		glog.Fatalf("Prometheus monitoring failed: %v", http.ListenAndServe(*prometheusEndpoint, nil))
	}()

	stopCh := newSystemStopChannel()
	eventExporter.Run(stopCh)
}
