package elasticsearch

import (
	api_v1 "k8s.io/api/core/v1"
)

type ElasticsearchEvent struct {
	event api_v1.Event

	Timestamp string
}
