package elasticsearch

import (
	"time"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	ReceivedEntryCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name:      "received_entry_count",
			Help:      "Number of events, received by the output sink",
			Subsystem: "output_sink",
		},
		[]string{"component"},
	)

	RequestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name:      "request_count",
			Help:      "Number of request, issued to output sink",
			Subsystem: "output_sink",
		},
		[]string{"code", "component"},
	)

	SuccessfullySentEntryCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name:      "successfully_sent_entry_count",
			Help:      "Number of events, successfully ingested by output sink",
			Subsystem: "output_sink",
		},
	)

	RecordLatency = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:      "records_latency_seconds",
			Help:      "Log entry latency between log timestamp and delivery to ouput sink.",
			Subsystem: "output_sink",
			// Highest bucket start at 2 sec * 1.5^19 = 4433.68 sec
			Buckets: prometheus.ExponentialBuckets(2, 1.5, 20),
		},
	)
)

func init() {
	prometheus.MustRegister(
		ReceivedEntryCount,
		RequestCount,
		SuccessfullySentEntryCount,
		RecordLatency,
	)
}

func measureLatencyOnSuccess(entries []*ElasticsearchEvent) {
	samples := make([]time.Time, 0)
	for _, e := range entries {
		// Do not measure latency if a log entry does not have a timestamp.
		if e.Timestamp == "" {
			continue
		}
		t, err := time.Parse(time.RFC3339Nano, e.Timestamp)
		if err != nil {
			glog.Warningf("Failed to parse timestamp: %s", e.Timestamp)
			continue
		}
		samples = append(samples, t)
	}
	now := time.Now()
	for _, ts := range samples {
		RecordLatency.Observe(now.Sub(ts).Seconds())
	}
}
